import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { MatSidenavModule } from '@angular/material/sidenav';

import { AppComponent } from './app.component';
import { TopContentModule } from './modules/top-content/top-content.module';
import { LeftContentModule } from './modules/left-content/left-content.module';
import { MiddleContentModule } from './modules/middle-content/middle-content.module';

import { AppRouter } from './app.router';
import { TopContentActionService } from './services/top-content-action-service/top-content-action.service';
import { TrackerService } from './services/tracker-service/tracker.service';
import { HttpClientModule } from '@angular/common/http';
import { TrackerActionService } from './services/tracker-action-service/tracker-action.service';
import { AppConfiguration } from './services/app-configuration-service/app-configuration';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRouter,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,

    MatSidenavModule,

    TopContentModule,
    LeftContentModule,
    MiddleContentModule
  ],
  providers: [TopContentActionService, TrackerService, TrackerActionService, AppConfiguration, HttpClientModule,
    { provide: APP_INITIALIZER, useFactory: (config: AppConfiguration) => () => config.load(), deps: [AppConfiguration], multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
