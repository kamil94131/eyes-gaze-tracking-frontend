import { Injectable } from '@angular/core';
import { GreyScaleEyes } from '../../models/tracker/grey-scale-eyes';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { NeuralNetworkConfiguration } from '../../utils/configuration/neural-network-configuration';
import { ApplicationConfiguration } from '../../utils/configuration/application-configuration';

@Injectable()
export class TrackerService {
  private serverURL: string = 'http://localhost:8080';
  private static readonly SAVE_GREY_SCALE_EYES = '/saveGreyScaleEyes';
  private static readonly SAVE_GREY_SCALE_MERGED_EYES = '/saveMergedGreyScaleEyes';
  private static readonly PREDICT_GAZE_POSITION = '/predictGazePosition';
  private static readonly START_LEARNING_PROCESS = '/startLearningProcess';
  private static readonly LOAD_NEURAL_NETWORK = '/loadLearnedNeuralNetwork';
  private static readonly GET_NEURAL_NETWORK_CONFIGURATION = '/getNeuralNetworkConfiguration';
  private static readonly CHANGE_NEURAL_NETWORK_CONFIGURATION = '/changeNeuralNetworkConfiguration';
  private static readonly GET_APPLICATION_CONFIGURATION = '/getApplicationConfiguration';
  private static readonly CHANGE_APPLICATION_CONFIGURATION = '/changeApplicationConfiguration';
  private static readonly IS_NEURAL_NETWORK_MODEL_TRAINED = '/isNeuralNetworkModelTrained';
  private static readonly GET_MERGED_EYES_GENERATED_FOLDERS = '/getMergedEyesGeneratedFolders';
  private static readonly GET_CORSS_VALIDATION_RESULT = '/getCrossValidationResult';
  private static readonly PING = '/ping';

  constructor(private http: HttpClient) {}

  public saveGreyScaleEyes(greyScaleEyes: Array<GreyScaleEyes>): Observable<Object> {
    return this.http.post(this.serverURL + TrackerService.SAVE_GREY_SCALE_EYES, greyScaleEyes);
  }

  public saveMergedGreyScaleEyes(greyScaleEyes: Array<GreyScaleEyes>): Observable<Object> {
    return this.http.post(this.serverURL + TrackerService.SAVE_GREY_SCALE_MERGED_EYES, greyScaleEyes);
  }

  public predictGazePosition(greyScaleEyes: GreyScaleEyes): Observable<Object> {
    return this.http.post(this.serverURL + TrackerService.PREDICT_GAZE_POSITION, greyScaleEyes);
  }

  public startLearningProcess(): Observable<Object> {
    return this.http.get(this.serverURL + TrackerService.START_LEARNING_PROCESS);
  }

  public loadLearnedNeuralNetwork(): Observable<Object> {
    return this.http.get(this.serverURL + TrackerService.LOAD_NEURAL_NETWORK);
  }

  public getNeuralNetworkConfiguration(): Observable<Object> {
    return this.http.get(this.serverURL + TrackerService.GET_NEURAL_NETWORK_CONFIGURATION);
  }

  public changeNeuralNetworkConfiguration(neuralNetworkConfiguration: NeuralNetworkConfiguration): Observable<Object> {
    return this.http.post(this.serverURL + TrackerService.CHANGE_NEURAL_NETWORK_CONFIGURATION, neuralNetworkConfiguration);
  }

  public getApplicationConfiguration(): Observable<Object> {
    return this.http.get(this.serverURL + TrackerService.GET_APPLICATION_CONFIGURATION);
  }

  public changeApplicationConfiguration(applicationConfiguration: ApplicationConfiguration): Observable<Object> {
    return this.http.post(this.serverURL + TrackerService.CHANGE_APPLICATION_CONFIGURATION, applicationConfiguration);
  }

  public isNeuralNetworkModelTrained(): Observable<Object> {
    return this.http.get(this.serverURL + TrackerService.IS_NEURAL_NETWORK_MODEL_TRAINED);
  }

  public getMergedEyesGeneratedFolders(): Observable<Object> {
    return this.http.get(this.serverURL + TrackerService.GET_MERGED_EYES_GENERATED_FOLDERS);
  }

  public getCrossValidationResult(): Observable<Object> {
    return this.http.get(this.serverURL + TrackerService.GET_CORSS_VALIDATION_RESULT);
  }

  public ping(): Observable<Object> {
    return this.http.get(this.serverURL + TrackerService.PING);
  }
}
