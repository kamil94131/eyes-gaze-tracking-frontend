import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AppConfiguration {
    private componentMessages: Object;
    private stringResources: Object;

    constructor(public http: HttpClient) {}

    public load(): void {
        this.loadResourceFile('../../assets/component-messages.json').subscribe(file => {
            this.componentMessages = file;
        });
        this.loadResourceFile('../../assets/strings-resource.json').subscribe(file => {
            this.stringResources = file;
        });
    }

    public getComponentsMessage(key: string): string {
        return this.componentMessages[key];
    }

    public getStringResource(key: string): string {
        return this.stringResources[key];
    }

    private loadResourceFile(path: string): Observable<Object>{
        return this.http.get(path);
    } 
}