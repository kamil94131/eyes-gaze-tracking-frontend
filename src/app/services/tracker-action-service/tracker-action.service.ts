import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { GreyScaleEyes } from '../../models/tracker/grey-scale-eyes';
import { EyeDataRequest } from './eye-data-request';

@Injectable()
export class TrackerActionService {
    private data: Subject<GreyScaleEyes> = new Subject<GreyScaleEyes>();
    private confirmation: Subject<EyeDataRequest> = new Subject<EyeDataRequest>();
    private allowDataSend: Subject<boolean> = new Subject<boolean>();

    public sendDetectedEyes(greyScaleEyes: GreyScaleEyes): void {
        this.data.next(greyScaleEyes);
    }

    public getDetectedEyes(): Observable<GreyScaleEyes> {
        return this.data.asObservable();
    } 

    public sendDetectedEyesRequest(eyeDataRequest: EyeDataRequest): void {
        this.confirmation.next(eyeDataRequest);
    }

    public getDetectedEyesRequest(): Observable<EyeDataRequest> {
        return this.confirmation.asObservable();
    } 

    public sendAllowDataEmit(allow: boolean): void {
        this.allowDataSend.next(allow);
    }

    public getAllowDataEmit(): Observable<boolean> {
        return this.allowDataSend.asObservable();
    }
}
