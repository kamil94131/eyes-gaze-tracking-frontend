export class EyeDataRequest {
    private _eyeId: number;
    private _dataRequest: boolean;

    constructor(eyeId: number, dataRequest: boolean){
        this._eyeId = eyeId;
        this._dataRequest = dataRequest;
    }

    public get eyeId(): number {
        return this._eyeId;
    }
    
    public set eyeId(value: number) {
        this._eyeId = value;
    }

    public get dataRequest(): boolean {
        return this._dataRequest;
    }

    public set dataRequest(value: boolean) {
        this._dataRequest = value;
    }
}