export class ComponentAction {
    private _action: any;
	private _value: any;
	
	constructor(action: any, value: any){
		this._action = action;
		this._value = value;
	}

	public get action(): any {
		return this._action;
	}

	public set action(action: any) {
		this._action = action;
	}
    
	public get value(): any {
		return this._value;
	}

	public set value(value: any) {
		this._value = value;
	}  
}