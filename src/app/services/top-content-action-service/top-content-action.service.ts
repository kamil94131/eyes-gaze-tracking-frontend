import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import { ComponentAction } from '../component-action';

@Injectable()
export class TopContentActionService {
  private subject = new Subject<any>();

  public sendAction(componentAction: ComponentAction) {
    this.subject.next(componentAction);
  }

  public getAction(): Observable<any> {
    return this.subject.asObservable();
  } 
}
