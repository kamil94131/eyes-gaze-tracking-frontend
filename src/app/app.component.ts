import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';
import { Component, ViewChild, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';

import { TopContentActionService } from './services/top-content-action-service/top-content-action.service';
import { TopContentActions } from './utils/component-actions/top-content-actions';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
    @ViewChild('matDrawer') matDrawer: MatDrawer;
    private subscription: Subscription;

    constructor(private router: Router, private topContentActionService: TopContentActionService){
        this.listenOnHideLeftContent();
    }

    ngOnInit(){
        this.navigateHomePage();
    }

    private navigateHomePage(){
        this.router.navigate(['video']);
    }

    private listenOnHideLeftContent(): void {
        this.subscription = this.topContentActionService.getAction().subscribe(componentAction => {   
            this.handleTopContentAction(componentAction.action, componentAction.value);
        });
    }

    private handleTopContentAction(action: any, value: any): void {
        switch(action){       
        case TopContentActions.UPDATE_LEFT_CONTENT:
            if(value){
                this.matDrawer.open();
            }else{
                this.matDrawer.close();
            }   
        break;
        case TopContentActions.OPEN_LEFT_CONTENT:
            this.matDrawer.open();
        break;
        }
    }
}