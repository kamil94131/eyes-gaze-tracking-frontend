import { Shape } from "../shape/shape";
import { Rectangle } from "./rectangle";

export class RectangleBuilder extends Shape {
    private _width: number;
    private _height: number;

    public setContext(context: CanvasRenderingContext2D): RectangleBuilder {
        this.context = context;
        return this;
    }

    public setId(id: number): RectangleBuilder {
        this.id = id; 
        return this;
    }

    public setX(x: number): RectangleBuilder {
        this.x = x;
        return this;
    }

    public setY(y: number): RectangleBuilder {
        this.y = y;
        return this;
    }

    public setVisible(visible: boolean): RectangleBuilder {
        this.visible = visible;
        return this;
    }

    public setEnabled(enable: boolean): RectangleBuilder {
        this.enable = enable;
        return this;
    }

    public setColors(colors: Array<string>): RectangleBuilder {
        this.colors = colors;
        return this;
    }

    public setColorIndex(colorIndex: number): RectangleBuilder {
        this.colorIndex = colorIndex;
        return this;
    }

    public setWidth(width: number): RectangleBuilder {
        this._width = width;
        return this;
    }

    public get width() {
        return this._width;
    }

    public setHeight(height: number): RectangleBuilder {
        this._height = height;
        return this;
    }

    public get height() {
        return this._height;
    }

    public setLineWidth(lineWidth: number): RectangleBuilder {
        this.lineWidth = lineWidth;
        return this;
    }

    public setStrokeColor(strokeColor: string): RectangleBuilder {
        this.strokeColor = strokeColor;
        return this;
    }

    public build(): Rectangle {
        return new Rectangle(this);
    }
}