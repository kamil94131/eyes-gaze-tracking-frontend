
import { ColorsUtil } from "../../../utils/configuration/colors-util";
import { RectangleBuilder } from "./rectangle-builder";
import { Shape } from "../shape/shape";
import { BaseShape } from "../shape/base-shape";

export class Rectangle extends Shape implements BaseShape {
    private _width: number;
    private _height: number;

    constructor(rectangleBuilder: RectangleBuilder){
        super();
        this.context = rectangleBuilder.context;
        this.id = rectangleBuilder.id
        this.x = rectangleBuilder.x;
        this.y = rectangleBuilder.y;
        this.visible = rectangleBuilder.visible;
        this.enable = rectangleBuilder.enable;
        this.colors = rectangleBuilder.colors;
        this.colorIndex = rectangleBuilder.colorIndex;
        this.lineWidth = rectangleBuilder.lineWidth;
        this._width = rectangleBuilder.width;
        this._height = rectangleBuilder.height;
        this.initialize();
    }

    public initialize(): void {
        this.initializeDefaultColors();
    }

    private initializeDefaultColors(){
        this.addColor(ColorsUtil.GREY);
        this.addColor(ColorsUtil.RED);
    }

    public draw(): void {
        if(this.visible){
            this.context.fillStyle = this.colors[this.colorIndex];
            this.context.fillRect(this.x, this.y, this._width, this._height);
            this.context.lineWidth = this.lineWidth;
            this.context.strokeStyle = this.strokeColor;
            this.context.strokeRect(this.x, this.y, this._width, this._height);
        }
    }

    public clear(): void {
        this.context.clearRect(this.x - this.lineWidth, 
                               this.y - this.lineWidth, 
                               this._width + 2 * this.lineWidth, 
                               this._height + 2 * this.lineWidth);
    }

    public set width(width: number) {
        this._width = width;
    }

    public get width() {
        return this._width;
    }

    public set height(height: number) {
        this._height = height;
    }

    public get height() {
        return this._height;
    }
}