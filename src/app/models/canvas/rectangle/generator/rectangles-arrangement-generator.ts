import { RectanglesGenerator } from "./rectangles-generator";
import { Rectangle } from "../rectangle";

export class RectanglesArrangementGenerator {
    public static generateRectangles(rectanglesGenerator: RectanglesGenerator): Array<Rectangle> {
        return rectanglesGenerator.generate();
    }

    public static reorganizeRectangles(rectanglesGenerator: RectanglesGenerator, rectanglesToReorganize: Array<Rectangle>){
        rectanglesGenerator.reorganize(rectanglesToReorganize);
    }
}