import { RectanglesDrawingStrategy } from "./rectangles-drawing-strategy";

export class RectanglesDrawingStrategyBuilder {
    private _context: CanvasRenderingContext2D;
    private _lineWidth: number;
    private _rectanglesInRow: number;
    private _rectanglesInColumn: number;
    private _canvasHeight: number;
    private _canvasWidth: number;

    constructor(context: CanvasRenderingContext2D){
        this._context = context;
    }

    public setRectanglesInRow(rectanglesInRow: number): RectanglesDrawingStrategyBuilder {
        this._rectanglesInRow = rectanglesInRow;
        return this;
    }

    public get rectanglesInRow(): number {
        return this._rectanglesInRow;
    }

    public setRectanglesInColumn(rectanglesInColumn: number): RectanglesDrawingStrategyBuilder {
        this._rectanglesInColumn = rectanglesInColumn;
        return this;
    }

    public get rectanglesInColumn(): number {
        return this._rectanglesInColumn;
    }

    public setCanvasHeight(canvasHeight: number): RectanglesDrawingStrategyBuilder {
        this._canvasHeight = canvasHeight;
        return this;
    }

    public get canvasHeight(): number {
        return this._canvasHeight;
    }

    public setCanvasWidth(canvasWidth: number): RectanglesDrawingStrategyBuilder {
        this._canvasWidth = canvasWidth;
        return this;
    }

    public get canvasWidth(): number {
        return this._canvasWidth;
    }

    public setLineWidth(lineWidth: number): RectanglesDrawingStrategyBuilder {
        this._lineWidth = lineWidth;
        return this;
    }

    public get lineWidth(): number{
        return this._lineWidth;
    }

    public get context(): CanvasRenderingContext2D {
        return this._context;
    }

    public build(): RectanglesDrawingStrategy {
        return new RectanglesDrawingStrategy(this);
    }
}