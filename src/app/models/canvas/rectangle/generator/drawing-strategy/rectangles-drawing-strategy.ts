import { RectanglesGenerator } from "../rectangles-generator";
import { Rectangle } from "../../rectangle";
import { RectanglesDrawingStrategyBuilder } from "./rectangles-drawing-strategy-builder";
import { RectangleBuilder } from "../../rectangle-builder";
import { ColorsUtil } from "../../../../../utils/configuration/colors-util";

export class RectanglesDrawingStrategy implements RectanglesGenerator {
    private _context: CanvasRenderingContext2D;
    private _lineWidth: number;
    private _rectanglesInRow: number;
    private _rectanglesInColumn: number;
    private _canvasHeight: number;
    private _canvasWidth: number;

    constructor(rectanglesDrawingStrategyBuilder: RectanglesDrawingStrategyBuilder){
        this._context = rectanglesDrawingStrategyBuilder.context;
        this._lineWidth = rectanglesDrawingStrategyBuilder.lineWidth;
        this._rectanglesInRow = rectanglesDrawingStrategyBuilder.rectanglesInRow;
        this._rectanglesInColumn = rectanglesDrawingStrategyBuilder.rectanglesInColumn;
        this._canvasHeight = rectanglesDrawingStrategyBuilder.canvasHeight;
        this._canvasWidth = rectanglesDrawingStrategyBuilder.canvasWidth;
    }

    public generate(): Array<Rectangle>{
        var rectangles: Array<Rectangle> = new Array<Rectangle>();
        var columnPosition: number = 0;
        var rowPosition: number = 0;
        var columnRange = (this._canvasWidth / this._rectanglesInColumn);
        var rowRange = (this._canvasHeight / this._rectanglesInRow);
        var rectangleId: number = 0;

        for(var rowIndex = 0; rowIndex < this._rectanglesInRow; rowIndex++){   
            for(var columnIndex = 0; columnIndex < this._rectanglesInColumn; columnIndex++){ 
                rectangles.push(new RectangleBuilder().setId(rectangleId)
                                                      .setContext(this._context)
                                                      .setX(columnPosition)
                                                      .setY(rowPosition)
                                                      .setWidth(columnRange)
                                                      .setHeight(rowRange)
                                                      .setEnabled(false)
                                                      .setLineWidth(this._lineWidth)
                                                      .setStrokeColor(ColorsUtil.BLACK)
                                                      .build());
                columnPosition += columnRange;  
                rectangleId++; 
            }
            columnPosition = 0;
            rowPosition += rowRange;
        }
        return rectangles;
    }

    public reorganize(rectanglesToReorganize: Array<Rectangle>): void {
        var columnPosition: number = 0;
        var rowPosition: number =  0;
        var columnRange = (this._canvasWidth / this._rectanglesInColumn);
        var rowRange = (this._canvasHeight / this._rectanglesInRow);

        for(let rowIndex = 0; rowIndex < this._rectanglesInRow; rowIndex++){   
            for(let columnIndex = 0; columnIndex < this._rectanglesInColumn; columnIndex++){
                rectanglesToReorganize[rowIndex * this._rectanglesInRow + columnIndex].x = columnPosition;
                rectanglesToReorganize[rowIndex * this._rectanglesInRow + columnIndex].y = rowPosition;
                columnPosition += columnRange;  
            }
            columnPosition = 0;
            rowPosition += rowRange;
        }
    }
}