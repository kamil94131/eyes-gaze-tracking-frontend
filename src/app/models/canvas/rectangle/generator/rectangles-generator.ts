import { Rectangle } from "../rectangle";

export interface RectanglesGenerator {
    generate(): Array<Rectangle>;
    reorganize(rectanglesToReorganize: Array<Rectangle>): void;
}