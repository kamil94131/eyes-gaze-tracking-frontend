export interface IShape {
    nextColor(): void;
    hasNextColor(): boolean
    previousColor(): void;
    addColor(color: string): void;
    removeColor(color: string): void;
    removeColor(index: number): void;
    resetToDefaultColor(): void;
    resetToDisabledColor(): void;
}