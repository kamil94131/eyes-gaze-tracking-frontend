export interface BaseShape {
    initialize(): void;
    clear(): void;
    draw(): void;
}