import { IShape } from './i-shape';
import { ShapeConfiguration } from './shape-configuration';

export abstract class Shape implements IShape {
	private _context: CanvasRenderingContext2D;
	private _id: number;
    private _x: number;
	private _y: number;
	private _visible: boolean = true;
	private _enable: boolean = true;
	private _colors: Array<string> = new Array<string>();
	private _colorIndex: number = 0;
	private _lineWidth: number;
	private _strokeColor: string;
	
	public nextColor(): void {
		if(this._colorIndex < this._colors.length){
			this._colorIndex++;
		}
	}

	public hasNextColor(): boolean{
		return this._colorIndex != this._colors.length - 1;
	}

	public previousColor(): void {
		if(this._colorIndex > ShapeConfiguration.MIN_COLOR_INDEX){
			this._colorIndex--;
		}
	}

	public addColor(color: string): void {
		this._colors.push(color);
	}
	
	public removeColor(parameter: any) {
		if(typeof parameter === 'string'){
			var colorIndex = this._colors.findIndex(color => color === parameter);
			if(colorIndex != -1){
				this._colors.splice(colorIndex, 1);
			}
		}else if(typeof parameter === 'number'){
			this._colors.splice(parameter, 1);
		}
	}

	public resetToDisabledColor(): void {
		this._colorIndex = 0;
	}

	public resetToDefaultColor(): void {
		this._colorIndex = 1;
	}

	public get context(): CanvasRenderingContext2D {
		return this._context;
	}
	public set context(context: CanvasRenderingContext2D) {
		this._context = context;
	}

	public get id() {
		return this._id;
    }

    public set id(id: number){
        this._id = id;
	}
	
	public get x(): number {
		return this._x;
	}

	public set x(x: number) {
		this._x = x;
	}

	public get y(): number {
		return this._y;
	}

	public set y(y: number) {
		this._y = y;
	}

	public get visible() {
		return this._visible;
	}

	public set visible(visible: boolean) {
		this._visible = visible;
	}

	public get enable() {
		return this._enable;
	}

	public set enable(enable: boolean) {
		this._enable = enable;
	}

	public get colors(): Array<string> {
		return this._colors;
	}

	public set colors(colors: Array<string>) {
		this._colors = colors;
	}

	public get colorIndex(): number {
		return this._colorIndex;
	}

	public set colorIndex(colorIndex: number) {
		this._colorIndex = colorIndex;
	}

	public get lineWidth(): number {
		return this._lineWidth;
	}

	public set lineWidth(lineWidth: number) {
		this._lineWidth = lineWidth;
	}

	public get strokeColor(): string {
		return this._strokeColor;
	}

	public set strokeColor(strokeColor: string) {
		this._strokeColor = strokeColor;
	}
}