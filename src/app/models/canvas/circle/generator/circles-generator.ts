import { Circle } from "../circle";

export interface CirclesGenerator {
    generate(): Array<Circle>;
    reorganize(circlesToReorganize: Array<Circle>): void;
}