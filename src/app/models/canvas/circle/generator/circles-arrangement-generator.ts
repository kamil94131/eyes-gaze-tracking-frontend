import { CirclesGenerator } from "./circles-generator";
import { Circle } from "../circle";

export class CirclesArrangementGenerator {
    public static generateCircles(circlesGenerator: CirclesGenerator): Array<Circle>{
        return circlesGenerator.generate();
    }

    public static reorganizeCircles(circlesGenerator: CirclesGenerator, circlesToReorganize: Array<Circle>): void{
        circlesGenerator.reorganize(circlesToReorganize);
    }
}