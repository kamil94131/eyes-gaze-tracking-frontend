import { CirclesDrawingStrategy } from "./circles-drawing-strategy";

export class CirclesDrawingStrategyBuilder {
    private _context: CanvasRenderingContext2D;
    private _radius: number;
    private _lineWidth: number;
    private _circlesInRow: number;
    private _circlesInColumn: number;
    private _canvasHeight: number;
    private _canvasWidth: number;

    constructor(context: CanvasRenderingContext2D){
        this._context = context;
    }

    public setRadius(radius: number): CirclesDrawingStrategyBuilder {
        this._radius = radius;
        return this;
    }

    public get radius(): number {
        return this._radius;
    }

    public setLineWidth(lineWidth: number): CirclesDrawingStrategyBuilder {
        this._lineWidth = lineWidth;
        return this;
    }

    public get lineWidth(): number {
        return this._lineWidth;
    }

    public setCirclesInRow(circlesInRow: number): CirclesDrawingStrategyBuilder {
        this._circlesInRow = circlesInRow;
        return this;
    }

    public get circlesInRow(): number {
        return this._circlesInRow;
    }

    public setCirclesInColumn(circlesInColumn: number): CirclesDrawingStrategyBuilder {
        this._circlesInColumn = circlesInColumn;
        return this;
    }

    public get circlesInColumn(): number {
        return this._circlesInColumn;
    }

    public setCanvasHeight(canvasHeight: number): CirclesDrawingStrategyBuilder {
        this._canvasHeight = canvasHeight;
        return this;
    }

    public get canvasHeight(): number {
        return this._canvasHeight;
    }

    public setCanvasWidth(canvasWidth: number): CirclesDrawingStrategyBuilder {
        this._canvasWidth = canvasWidth;
        return this;
    }

    public get canvasWidth(): number {
        return this._canvasWidth;
    }

    public get context(): CanvasRenderingContext2D {
        return this._context;
    }

    public build(): CirclesDrawingStrategy {
        return new CirclesDrawingStrategy(this);
    }
}