import { CirclesGenerator } from "../circles-generator";
import { Circle } from "../../circle";
import { CirclesDrawingStrategyBuilder } from "./circles-drawing-strategy-builder";
import { CircleBuilder } from "../../circle-builder";
import { ColorsUtil } from "../../../../../utils/configuration/colors-util";
import { CircleConfiguration } from "../../circle-configuration";

export class CirclesDrawingStrategy implements CirclesGenerator {
    private _context: CanvasRenderingContext2D;
    private _radius: number;
    private _lineWidth: number;
    private _circlesInRow: number;
    private _circlesInColumn: number;
    private _canvasHeight: number;
    private _canvasWidth: number;

    constructor(circlesDrawingStrategyBuilder: CirclesDrawingStrategyBuilder){
        this._context = circlesDrawingStrategyBuilder.context;
        this._radius = circlesDrawingStrategyBuilder.radius;
        this._lineWidth = circlesDrawingStrategyBuilder.lineWidth;
        this._circlesInRow = circlesDrawingStrategyBuilder.circlesInRow;
        this._circlesInColumn = circlesDrawingStrategyBuilder.circlesInColumn;
        this._canvasHeight = circlesDrawingStrategyBuilder.canvasHeight;
        this._canvasWidth = circlesDrawingStrategyBuilder.canvasWidth;
    }

    public generate(): Array<Circle> {
        var circles: Array<Circle> = new Array<Circle>();
        var columnPosition: number = this._radius + this._lineWidth;
        var rowPosition: number =  this._radius + this._lineWidth;
        var columnRange = this._canvasWidth / (this._circlesInColumn - 1);
        var rowRange = this._canvasHeight / (this._circlesInRow - 1);
        var circleId: number = 0;

        for(let rowIndex = 0; rowIndex < this._circlesInRow; rowIndex++){   
            for(let columnIndex = 0; columnIndex < this._circlesInColumn; columnIndex++){
                circles.push(new CircleBuilder().setId(circleId)
                                                .setContext(this._context)
                                                .setX(columnPosition)
                                                .setY(rowPosition)
                                                .setRadius(this._radius)
                                                .setEnable(false)
                                                .setLineWidth(this._lineWidth)
                                                .setStrokeColor(ColorsUtil.BLACK)
                                                .build());            
                columnPosition += columnRange - this._radius - this._lineWidth;
                circleId++;
            }
            rowPosition += rowRange - this._radius - this._lineWidth;
            columnPosition = this._radius + this._lineWidth;
        }
        return circles;
    }

    public reorganize(circlesToReorganize: Array<Circle>): void {
        var columnPosition: number = this._radius + this._lineWidth;
        var rowPosition: number =  this._radius + this._lineWidth;
        var columnRange = this._canvasWidth / (this._circlesInColumn - 1);
        var rowRange = this._canvasHeight / (this._circlesInRow - 1);

        for(let rowIndex = 0; rowIndex < this._circlesInRow; rowIndex++){   
            for(let columnIndex = 0; columnIndex < this._circlesInColumn; columnIndex++){
                circlesToReorganize[rowIndex * this._circlesInRow + columnIndex].x = columnPosition;
                circlesToReorganize[rowIndex * this._circlesInRow + columnIndex].y = rowPosition;
                columnPosition += columnRange - this._radius - this._lineWidth;
            }
            rowPosition += rowRange - this._radius - this._lineWidth;
            columnPosition = this._radius + this._lineWidth;
        } 
    }
}