import { ColorsUtil } from '../../../utils/configuration/colors-util';
import { CircleBuilder } from './circle-builder';
import { Shape } from '../shape/shape';
import { BaseShape } from '../shape/base-shape';
import { CircleConfiguration } from './circle-configuration';

export class Circle extends Shape implements BaseShape {
    private _radius: number;

    constructor(circleBuilder: CircleBuilder){
        super();
        this.context = circleBuilder.context;
        this.id = circleBuilder.id
        this.x = circleBuilder.x;
        this.y = circleBuilder.y;
        this.visible = circleBuilder.visible;
        this.enable = circleBuilder.enable;
        this.colors = circleBuilder.colors;
        this.colorIndex = circleBuilder.colorIndex;
        this._radius = circleBuilder.radius;
        this.lineWidth = circleBuilder.lineWidth;
        this.strokeColor = circleBuilder.strokeColor;
        this.initialize();
    }

    public initialize(): void {
        this.initializeDefaultColors();
    }    

    private initializeDefaultColors(): void {
        this.addColor(ColorsUtil.GREY);
        this.addColor(ColorsUtil.RED);
        this.addColor(ColorsUtil.YELLOW);
    }
    
    public draw(): void {
        if(this.visible){
            this.context.beginPath();
            this.context.arc(this.x, this.y, this._radius, CircleConfiguration.START_ANGLE, CircleConfiguration.END_ANGLE * Math.PI, false);
            this.context.fillStyle = this.colors[this.colorIndex];
            this.context.fill();
            this.context.lineWidth = this.lineWidth;
            this.context.strokeStyle = this.strokeColor;
            this.context.stroke();
        }
    }
    
    public clear(): void {
        this.context.clearRect(this.x - this.radius - this.lineWidth,
                               this.y - this.radius - this.lineWidth, 
                               2 * this.radius + 2 * this.lineWidth, 
                               2 * this.radius + 2 * this.lineWidth);
    }

    public set radius(radius: number){
        this._radius = radius;
    }

    public get radius(){
        return this._radius;
    }
}