import { Shape } from '../shape/shape';
import { Circle } from './circle';

export class CircleBuilder extends Shape {
    private _radius: number;

    public setContext(context: CanvasRenderingContext2D): CircleBuilder {
        this.context = context;
        return this;
    }

    public setId(id: number): CircleBuilder {
        this.id = id; 
        return this;
    }

    public setX(x: number): CircleBuilder {
        this.x = x;
        return this;
    }

    public setY(y: number): CircleBuilder {
        this.y = y;
        return this;
    }

    public setVisible(visible: boolean): CircleBuilder {
        this.visible = visible;
        return this;
    }

    public setEnable(enable: boolean): CircleBuilder {
        this.enable = enable;
        return this;
    }

    public setColors(colors: Array<string>): CircleBuilder {
        this.colors = colors;
        return this;
    }

    public setColorIndex(colorIndex: number): CircleBuilder {
        this.colorIndex = colorIndex;
        return this;
    }

    public setRadius(radius: number): CircleBuilder {
        this._radius = radius;
        return this;
    }

    public get radius(){
        return this._radius;
    }

    public setLineWidth(lineWidth: number): CircleBuilder {
        this.lineWidth = lineWidth;
        return this;
    }

    public setStrokeColor(strokeColor: string): CircleBuilder {
        this.strokeColor = strokeColor;
        return this;
    }
    
    public build(): Circle {
        return new Circle(this);
    }
}