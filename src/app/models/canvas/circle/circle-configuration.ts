export abstract class CircleConfiguration {
    public static readonly START_ANGLE: number = 0;
    public static readonly END_ANGLE: number = 2;
    public static readonly LINE_WIDTH: number = 5;
}