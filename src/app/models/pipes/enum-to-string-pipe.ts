import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'enumToStringPipe'})
export class EnumToStringPipe implements PipeTransform {
    transform(values: any, args: any[]): any {
        let items: any[] = [];

        for (let key in values){
            var isValueProperty = parseInt(key, 10) >= 0;
            if(isValueProperty){
                continue;
            } 
            items.push({key: key, value: values[key]});
        }
        return items;
    }
}