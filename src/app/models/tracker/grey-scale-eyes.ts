import { GreyScaleEyesBuilder } from "./grey-scale-eyes-builder";

export class GreyScaleEyes {
    private _eyeId: number;
    private _leftEye: Uint8ClampedArray;
    private _rightEye: Uint8ClampedArray;

    constructor(greyScaleEyesBuilder: GreyScaleEyesBuilder){
        this._eyeId = greyScaleEyesBuilder.id;
        this._leftEye = greyScaleEyesBuilder.leftEye;
        this._rightEye = greyScaleEyesBuilder.rightEye;    
    }

    public get eyeId(): number {
        return this._eyeId;
    }
    public set eyeId(eyeId: number) {
        this._eyeId = eyeId;
    }

    public get leftEye(): Uint8ClampedArray {
		return this._leftEye;
    }
    
	public set leftEye(rightEye: Uint8ClampedArray) {
		this._rightEye = rightEye;
    }

    public get rightEye(): Uint8ClampedArray {
        return this._rightEye;
    }
    public set rightEye(value: Uint8ClampedArray) {
        this._rightEye = value;
    }
}