import { GreyScaleEyes } from "./grey-scale-eyes";

export class GreyScaleEyesBuilder {
    private _eyeId: number;
    private _leftEye: Uint8ClampedArray;
    private _rightEye: Uint8ClampedArray;

    constructor(eyeId: number){
        this._eyeId = eyeId;
    }

    public setLeftEye(leftEye: Uint8ClampedArray): GreyScaleEyesBuilder {
        this._leftEye = leftEye;
        return this;
    }

    public get leftEye(): Uint8ClampedArray {
        return this._leftEye;
    }

    public setRightEye(rightEye: Uint8ClampedArray): GreyScaleEyesBuilder {
        this._rightEye = rightEye;
        return this;
    }

    public get rightEye(): Uint8ClampedArray {
        return this._rightEye;
    }

    public get id(): number {
        return this._eyeId;
    }

    public build(): GreyScaleEyes {
        return new GreyScaleEyes(this);
    }
}