import { ColorsUtil } from "../../utils/configuration/colors-util";
import { GreyScaleEyes } from "./grey-scale-eyes";
import { TrackerConfiguration } from "./tracker-configuration";
import { DataSendStartegy } from "./data-send-strategy";
import { GreyScaleEyesBuilder } from "./grey-scale-eyes-builder";
import { TrackerActionService } from "../../services/tracker-action-service/tracker-action.service";
import { Eye } from "./eye";
import { EyeDataRequest } from "../../services/tracker-action-service/eye-data-request";

export class Tracker {
    private _videoId: string;
    private _canvasId: string;
    private _trackerTask: any;
    private _request: boolean;
    private _eyeId: number;
    private _allowDataSend: boolean = false;
    private _trackerActionService: TrackerActionService;

    constructor(videoId: string, canvasId: string, trackerActionService?: TrackerActionService){
        this._videoId = videoId;
        this._canvasId = canvasId;
        this._trackerActionService = trackerActionService;
    }

    public run(dataSendStrategy: DataSendStartegy, displayDetectionBorders: boolean): void {
        this.manageListeners();

        var video = <HTMLVideoElement>document.getElementById(this._videoId);
        var canvas = <HTMLCanvasElement>document.getElementById(this._canvasId);
        var context = <CanvasRenderingContext2D>canvas.getContext(TrackerConfiguration.CONTEXT);
        context.strokeStyle = ColorsUtil.WHITE;

        var tracker = new tracking.ObjectTracker([TrackerConfiguration.TRACKED_OBJECT]);
        tracker.setStepSize(TrackerConfiguration.STEP_SIZE);      
        tracker.setEdgesDensity(TrackerConfiguration.EDGE_DENSITY);
        this._trackerTask = tracking.track(TrackerConfiguration.HASH + this._videoId, tracker, { camera: true });       

        tracker.on(TrackerConfiguration.TRACKING_TYPE, (event) => {            
            context.clearRect(0, 0, canvas.width, canvas.height); 
            context.drawImage(video, 0, 0, canvas.width, canvas.height);  
            
            if(event.data.length == TrackerConfiguration.DETECTED_BOTH_EYES){
                if(this._allowDataSend){
                    switch(dataSendStrategy){
                        case DataSendStartegy.CONSTANT:
                            this._trackerActionService.sendDetectedEyes(this.prepareDetectionData(context, event));
                        break;
                        case DataSendStartegy.REQUEST:
                            if(this._request){
                                this._request = false;
                                this._trackerActionService.sendDetectedEyes(this.prepareDetectionData(context, event));                     
                            }
                        break;
                    }
                }

                if(displayDetectionBorders){
                    this.drawDetectionBorders(context, event);
                } 
            }     
        });      
    }

    private manageListeners(): void {
        if(this._trackerActionService != undefined){
            this.listenOnAllowDataSend();
            this.listenOnDetectedEyesRequest();
        }         
    }

    private listenOnDetectedEyesRequest(): void {
        this._trackerActionService.getDetectedEyesRequest().subscribe(eyeDataRequest => {
            var request: EyeDataRequest = eyeDataRequest;
            this._eyeId = request.eyeId;
            this._request = request.dataRequest;
        });
    }

    private listenOnAllowDataSend(): void {
        this._trackerActionService.getAllowDataEmit().subscribe(allow => {
            this._allowDataSend = allow;
        });
    }

    private prepareDetectionData(context: CanvasRenderingContext2D, event: any): GreyScaleEyes {
        var leftEyeIndex: number = event.data[0].x < event.data[1].x ? Eye.LEFT : Eye.RIGHT;
        var leftRightIndex: number = event.data[0].x < event.data[1].x ? Eye.RIGHT : Eye.LEFT;
        var pixelsLeftEye = context.getImageData(event.data[leftEyeIndex].x + TrackerConfiguration.SHIFT_WIDTH, event.data[leftEyeIndex].y + TrackerConfiguration.SHIFT_HEIGHT , TrackerConfiguration.IMAGE_WIDTH, TrackerConfiguration.IMAGE_HEIGHT);
        var pixelsRightEye = context.getImageData(event.data[leftRightIndex].x + TrackerConfiguration.SHIFT_WIDTH, event.data[leftRightIndex].y + TrackerConfiguration.SHIFT_HEIGHT, TrackerConfiguration.IMAGE_WIDTH, TrackerConfiguration.IMAGE_HEIGHT);
        return new GreyScaleEyesBuilder(this._eyeId).setLeftEye(tracking.Image.grayscale(pixelsLeftEye.data, TrackerConfiguration.IMAGE_WIDTH, TrackerConfiguration.IMAGE_HEIGHT, false))
                                                    .setRightEye(tracking.Image.grayscale(pixelsRightEye.data, TrackerConfiguration.IMAGE_WIDTH, TrackerConfiguration.IMAGE_HEIGHT, false))
                                                    .build();
    }

    private drawDetectionBorders(context: CanvasRenderingContext2D, event: any){
        event.data.forEach((rect) => {   
            context.strokeRect(rect.x + TrackerConfiguration.SHIFT_WIDTH, rect.y + TrackerConfiguration.SHIFT_HEIGHT, TrackerConfiguration.IMAGE_WIDTH, TrackerConfiguration.IMAGE_HEIGHT);
        });
    }

    public shutdown(): void {
        this._trackerTask.stop();
    }

	public get videoId(): string {
		return this._videoId;
	}

	public set videoId(value: string) {
		this._videoId = value;
    }
    
	public get canvasId(): string {
		return this._canvasId;
	}

	public set canvasId(value: string) {
		this._canvasId = value;
    }
}