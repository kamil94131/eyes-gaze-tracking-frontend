export abstract class TrackerConfiguration {
    public static readonly DETECTED_BOTH_EYES: number = 2;
    public static readonly IMAGE_WIDTH: number = 36;
    public static readonly IMAGE_HEIGHT: number = 28;
    public static readonly SHIFT_HEIGHT: number = 6;
    public static readonly SHIFT_WIDTH: number = 4;
    public static readonly HASH: string = '#';
    public static readonly CONTEXT: string = '2d';
    public static readonly TRACKED_OBJECT: string = 'eye';
    public static readonly TRACKING_TYPE: string = 'track';
    public static readonly STEP_SIZE: number = 1.7;
    public static readonly EDGE_DENSITY: number = 0.1;
}