export class CrossValidationResult {
    private _folderId: number;
    private _folderQuantity: number;
    private _positiveEvaluation: number;
    private _negativeEvaluation: number;

    constructor(folderId: number, folderQuantity: number, positiveEvaluation: number, negativeEvaluation: number){
        this._folderId = folderId;
        this._folderQuantity = folderQuantity;
        this._positiveEvaluation = positiveEvaluation;
        this._negativeEvaluation = negativeEvaluation;
    }

	public get folderId(): number {
		return this._folderId;
    }
    
	public set folderId(value: number) {
		this._folderId = value;
	}

	public get folderQuantity(): number {
		return this._folderQuantity;
	}

	public set folderQuantity(value: number) {
		this._folderQuantity = value;
	}

	public get negativeEvaluation(): number {
		return this._negativeEvaluation;
	}

	public set negativeValuation(value: number) {
		this._negativeEvaluation = value;
    }
    
	public get positiveValuation(): number {
		return this._positiveEvaluation;
	}

	public set positiveValuation(value: number) {
		this._positiveEvaluation = value;
	}
}