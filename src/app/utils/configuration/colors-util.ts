export class ColorsUtil {
    public static readonly PURPLE: string = '#A64CEB';
    public static readonly WHITE: string = '#FFFFFF';
    public static readonly BLACK: string = '#000000';  
    public static readonly RED: string = '#FF0000'; 
    public static readonly YELLOW: string = '#FFFF00'; 
    public static readonly GREEN: string = '#008000'; 
    public static readonly GREY: string = '#B8B8B8';
    public static readonly BLUE: string = '#000080';
    public static readonly ORANGE: string = '#FF8C00';
}