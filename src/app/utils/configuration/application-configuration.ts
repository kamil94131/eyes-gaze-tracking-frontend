import { CollectingEyeDataMethod } from "./collecing-eye-data-method";
import { MergeDirection } from "./merge-direction";

export class ApplicationConfiguration {
    private _collectingEyeDataMethod: CollectingEyeDataMethod = CollectingEyeDataMethod.MERGED;
    private _mergeDirection: MergeDirection = MergeDirection.VERTICALY;
    private _useMedianFilter: boolean = true;

    public copy(applicationConfiguration: ApplicationConfiguration): void {
        this._collectingEyeDataMethod = applicationConfiguration.collectingEyeDataMethod;
        this._mergeDirection = applicationConfiguration.mergeDirection;
        this._useMedianFilter = applicationConfiguration.useMedianFilter;
    }

    public get collectingEyeDataMethod(): CollectingEyeDataMethod {
        return this._collectingEyeDataMethod;
    }

    public set collectingEyeDataMethod(value: CollectingEyeDataMethod) {
        this._collectingEyeDataMethod = value;
    }

    public get mergeDirection(): MergeDirection {
        return this._mergeDirection;
    }

    public set mergeDirection(value: MergeDirection) {
        this._mergeDirection = value;
    }

	public get useMedianFilter(): boolean  {
		return this._useMedianFilter;
	}

	public set useMedianFilter(value: boolean ) {
		this._useMedianFilter = value;
	}

}