export class NeuralNetworkConfiguration {
    private _channel: number = 1;
    private _rngseed: number = 0; 
    private _batchSize: number = 0;  
    private _outputNumber: number = 0; 
    private _numberEpochs: number = 0;
    private _learningRate: number = 0.0;  
    private _momentum: number = 0.0;
    
    public copy(neuralNetworkConfiguration: NeuralNetworkConfiguration): void {
        this._channel = neuralNetworkConfiguration.channel;
        this._rngseed = neuralNetworkConfiguration.rngseed;
        this._batchSize = neuralNetworkConfiguration.batchSize;
        this._outputNumber = neuralNetworkConfiguration.outputNumber;
        this._numberEpochs = neuralNetworkConfiguration.numberEpochs;
        this._learningRate = neuralNetworkConfiguration.learningRate;
        this._momentum = neuralNetworkConfiguration.momentum;
    }
    
    public get channel(): number {
        return this._channel;
    }

    public set channel(value: number) {
        this._channel = value;
    }

    public get rngseed(): number {
        return this._rngseed;
    }

    public set rngseed(rngseed: number) {
        this._rngseed = rngseed;
    }

    public get batchSize(): number {
        return this._batchSize;
    }

    public set batchSize(batchSize: number) {
        this._batchSize = batchSize;
    }

    public get outputNumber(): number {
        return this._outputNumber;
    }

    public set outputNumber(outputNumber: number) {
        this._outputNumber = outputNumber;
    }

    public get numberEpochs(): number {
        return this._numberEpochs;
    }

    public set numberEpochs(numberEpochs: number) {
        this._numberEpochs = numberEpochs;
    }

    public get learningRate(): number {
        return this._learningRate;
    }

    public set learningRate(value: number) {
        this._learningRate = value;
    }

    public get momentum(): number {
        return this._momentum;
    }

    public set momentum(value: number) {
        this._momentum = value;
    }
}