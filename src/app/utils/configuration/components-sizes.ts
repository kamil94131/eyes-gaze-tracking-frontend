export class ComponentsSizes {
    public static readonly LEFT_CONTENT_WIDTH: number = document.documentElement.clientWidth * 0.15; 
    public static readonly TOP_CONTENT_HEIGHT: number = document.documentElement.clientHeight * 0.105;
}