export enum CollectingEyeDataMethod {
    SEPARATED = 'SEPARATED',
    MERGED = 'MERGED'
}