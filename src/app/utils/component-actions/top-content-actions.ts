export class TopContentActions {
    public static readonly UPDATE_LEFT_CONTENT: number = 0;
    public static readonly CHANGE_LEFT_CONTENT_ICON: number = 1;
    public static readonly OPEN_LEFT_CONTENT: number = 2;
}