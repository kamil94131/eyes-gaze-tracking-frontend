import { Component, OnInit, OnDestroy } from '@angular/core';

import { TopContentActionService } from '../../services/top-content-action-service/top-content-action.service';
import { ComponentAction } from '../../services/component-action';
import { TopContentActions } from '../../utils/component-actions/top-content-actions';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-top-content',
    templateUrl: './top-content.component.html',
    styleUrls: ['./top-content.component.css']
})
export class TopContentComponent implements OnInit, OnDestroy {
    public readonly calibrationIsNotPossible: string  = 'Hide left content to start calibration process!'
    public readonly calibrationIsPossible: string  = 'Calibration process is ready!'
    private closeLeftContentIcon: string = 'arrow_back';
    public leftContentOpened: boolean = true;
    public calibrationViewEnter: boolean = false; 
    private hideLeftContentSubscription: Subscription;

    constructor(private topContentActionService: TopContentActionService) {}

    ngOnInit(): void {
        this.listenOnHideLeftContentEvent();
    }

    ngOnDestroy(): void {
        this.hideLeftContentSubscription.unsubscribe();
    }

    private onChangeLeftContentButtonClick(): void { 
        this.leftContentOpened = !this.leftContentOpened;
        this.closeLeftContentIcon = this.leftContentOpened ? 'arrow_back' : 'arrow_forward';
        this.topContentActionService.sendAction(new ComponentAction(TopContentActions.UPDATE_LEFT_CONTENT, this.leftContentOpened));
        
    }

    private onGoHomeButtonClick(): void {
        this.leftContentOpened = true
        this.closeLeftContentIcon = 'arrow_back';
        this.topContentActionService.sendAction(new ComponentAction(TopContentActions.UPDATE_LEFT_CONTENT, this.leftContentOpened));
    }

    private listenOnHideLeftContentEvent(): void {
        this.hideLeftContentSubscription = this.topContentActionService.getAction().subscribe(componentAction => {   
            this.handleTopContentEvent(componentAction.action, componentAction.value);
        });
    }

    private handleTopContentEvent(action: any, value: any): void {
        switch(action){       
            case TopContentActions.OPEN_LEFT_CONTENT:
                this.leftContentOpened = true;
                this.closeLeftContentIcon = 'arrow_back';
            break;
        }
    }
}
