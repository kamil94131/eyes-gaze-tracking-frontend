import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { LeftContentComponent } from './left-content.component';

import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,

    MatButtonModule,
    MatDividerModule,
    MatListModule,
    MatToolbarModule,
    MatIconModule
  ],
  exports: [LeftContentComponent],
  declarations: [LeftContentComponent]
})
export class LeftContentModule { }
