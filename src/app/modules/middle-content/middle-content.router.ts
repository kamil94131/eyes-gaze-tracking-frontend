import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

export const routes: Routes = [
    { path: 'video', loadChildren: './video/video.module#VideoModule' },
    { path: 'calibration', loadChildren: './calibration/calibration.module#CalibrationModule' },
    { path: 'test', loadChildren: './test/test.module#TestModule' },
    { path: 'configuration', loadChildren: './configuration/configuration.module#ConfigurationModule' },
    { path: 'crossValidation', loadChildren: './cross-validation/cross-validation.module#CrossValidationModule' }
];

export const MiddleContentRouter: ModuleWithProviders = RouterModule.forChild(routes);