import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MiddleContentComponent } from './middle-content.component';
import { MiddleContentRouter } from './middle-content.router';
import { CrossValidationComponent } from './cross-validation/cross-validation.component';

@NgModule({
  imports: [
    CommonModule,
    MiddleContentRouter   
  ],
  exports: [MiddleContentComponent],
  declarations: [MiddleContentComponent]
})
export class MiddleContentModule {}
