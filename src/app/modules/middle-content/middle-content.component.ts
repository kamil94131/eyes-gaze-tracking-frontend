import { Component, ViewEncapsulation } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationError, NavigationCancel } from '@angular/router';

@Component({
  selector: 'app-middle-content',
  templateUrl: './middle-content.component.html',
  styleUrls: ['./middle-content.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MiddleContentComponent {
  private loading: boolean = false;

  constructor(private router: Router) {
    this.initializeRouterEvent();
  }

  private initializeRouterEvent(): void{
    this.router.events.subscribe(event => {
        if(event instanceof NavigationStart) {
            this.loading = true;
        }else if(event instanceof NavigationEnd ||
                 event instanceof NavigationError ||
                 event instanceof NavigationCancel) {
            this.loading = false;
        }
    });
  } 
}
