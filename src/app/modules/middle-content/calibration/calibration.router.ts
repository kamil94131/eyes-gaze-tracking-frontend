import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CalibrationComponent } from "./calibration.component";

export const router: Routes = [
    { path: '', component: CalibrationComponent } 
  ];
  
export const CalibrationRouter: ModuleWithProviders = RouterModule.forChild(router);