import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CalibrationRouter } from './calibration.router';
import { CalibrationComponent } from './calibration.component';

@NgModule({
  imports: [
    CommonModule,
    CalibrationRouter,
    MatSnackBarModule
  ],
  declarations: [CalibrationComponent],
  exports:[CalibrationComponent]
})
export class CalibrationModule { }
