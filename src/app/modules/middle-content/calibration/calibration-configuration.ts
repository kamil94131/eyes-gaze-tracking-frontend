export abstract class CalibrationConfiguration {
    public static readonly CIRCLES_IN_ROW: number = 3;
    public static readonly CIRCLES_IN_COLUMN: number = 3;
    public static readonly RADIUS: number = 20; 
    public static readonly VIDEO_ID = 'video';
    public static readonly CANVAS_ID = 'canvas';
    public static readonly MESSAGE_DURATION = 1500;
}