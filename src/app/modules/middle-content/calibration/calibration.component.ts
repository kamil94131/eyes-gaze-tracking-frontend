import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy, NgZone } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { Circle } from '../../../models/canvas/circle/circle';
import { ComponentsSizes } from '../../../utils/configuration/components-sizes';
import { Tracker } from '../../../models/tracker/tracker';
import { TopContentActionService } from '../../../services/top-content-action-service/top-content-action.service';
import { TopContentActions } from '../../../utils/component-actions/top-content-actions';
import { MatSnackBar } from '@angular/material';
import { GreyScaleEyes } from '../../../models/tracker/grey-scale-eyes';
import { TrackerService } from '../../../services/tracker-service/tracker.service';
import { TrackerActionService } from '../../../services/tracker-action-service/tracker-action.service';
import { CirclesArrangementGenerator } from '../../../models/canvas/circle/generator/circles-arrangement-generator';
import { CalibrationConfiguration } from './calibration-configuration';
import { DataSendStartegy } from '../../../models/tracker/data-send-strategy';
import { CircleConfiguration } from '../../../models/canvas/circle/circle-configuration';
import { CirclesDrawingStrategyBuilder } from '../../../models/canvas/circle/generator/drawing-strategy/circles-drawing-strategy-builder';
import { EyeDataRequest } from '../../../services/tracker-action-service/eye-data-request';
import { AppConfiguration } from '../../../services/app-configuration-service/app-configuration';
import { ComponentAction } from '../../../services/component-action';
import { ApplicationConfiguration } from '../../../utils/configuration/application-configuration';
import { CollectingEyeDataMethod } from '../../../utils/configuration/collecing-eye-data-method';

@Component({
    selector: 'app-calibration',
    templateUrl: './calibration.component.html',
    styleUrls: ['./calibration.component.css']
})
export class CalibrationComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('circlesCanvas') canvas: ElementRef;
    private context: CanvasRenderingContext2D;
    private tracker: Tracker;
    private applicationConfiguration: ApplicationConfiguration = new ApplicationConfiguration();
    private hideLeftContentSubscription: Subscription;
    private detectedEyesRequestSubscription: Subscription;
    private circles: Array<Circle>;
    private leftMenuContentOpened: boolean = true;
    private finishedCirclesCounter: number = 0;
    private greyScaleEyes: Array<GreyScaleEyes> = new Array<GreyScaleEyes>();
    private loading: boolean = false;

    constructor(private ngZone: NgZone, 
                private topContentActionService: TopContentActionService,
                public message: MatSnackBar, 
                private trackerService: TrackerService, 
                private trackerActionService: TrackerActionService,
                private appConfiguration: AppConfiguration) {
    }

    ngOnInit() {
        this.tracker = new Tracker(CalibrationConfiguration.VIDEO_ID, CalibrationConfiguration.CANVAS_ID, this.trackerActionService);
        this.context = (this.canvas.nativeElement as HTMLCanvasElement).getContext("2d"); 
        this.getApplicationConfiguration();
        this.listenOnHideLeftContentEvent();
        this.listenOnDetectedEyesFeedback();
    }

    ngAfterViewInit(): void {
        setTimeout(() => this.showMessage(this.appConfiguration.getComponentsMessage('tracker.modules.middlecontent.calibration.text.cannotstart'), 
                                          this.appConfiguration.getComponentsMessage('tracker.modules.middlecontent.calibration.title.cannotstart')));
        this.ngZone.runOutsideAngular(() => {
            this.changeCanvasSize(window.innerWidth - ComponentsSizes.LEFT_CONTENT_WIDTH, window.innerHeight - ComponentsSizes.TOP_CONTENT_HEIGHT);
            this.createNewCircles();
            this.drawCircles();
            this.tracker.run(DataSendStartegy.REQUEST, true);
        });
    }

    ngOnDestroy(): void {
        this.message.dismiss();
        this.hideLeftContentSubscription.unsubscribe();
        this.detectedEyesRequestSubscription.unsubscribe();
        this.tracker.shutdown();   
    }

    private getApplicationConfiguration(): void {
        this.trackerService.ping().subscribe(
            data => {
                this.trackerService.getApplicationConfiguration().subscribe(
                    data => { this.applicationConfiguration.copy(<ApplicationConfiguration>data); },
                    error => { this.handleErrorPing(); }
                )
            },
            error => { this.handleErrorPing(); }
        );
    }

    private handleErrorPing(): void {
        setTimeout(() => this.showMessage(this.appConfiguration.getComponentsMessage('tracker.service.server.conncetion.text.error'), 
                                              this.appConfiguration.getComponentsMessage('tracker.service.server.conncetion.title.error')));
    }

    private showMessage(text: string, label: string): void {
        this.message.open(text, label, { duration: CalibrationConfiguration.MESSAGE_DURATION });
    }

    private changeCanvasSize(canvasWidth: number, canvasHeight: number){
        (this.canvas.nativeElement as HTMLCanvasElement).width  = canvasWidth;
        (this.canvas.nativeElement as HTMLCanvasElement).height = canvasHeight;
    }

    private clearCanvas(): void {
        this.context.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
    }

    private createNewCircles(): void {
        this.circles = CirclesArrangementGenerator.generateCircles(new CirclesDrawingStrategyBuilder(this.context)
                                                                                                    .setRadius(CalibrationConfiguration.RADIUS)
                                                                                                    .setLineWidth(CircleConfiguration.LINE_WIDTH)
                                                                                                    .setCirclesInRow(CalibrationConfiguration.CIRCLES_IN_ROW)
                                                                                                    .setCirclesInColumn(CalibrationConfiguration.CIRCLES_IN_COLUMN)
                                                                                                    .setCanvasWidth((this.canvas.nativeElement as HTMLCanvasElement).width)
                                                                                                    .setCanvasHeight((this.canvas.nativeElement as HTMLCanvasElement).height)
                                                                                                    .build());
        this.attachCirclesEvent();
    }

    private reorganizeCircles(): void {
        this.clearCanvas();
        CirclesArrangementGenerator.reorganizeCircles(new CirclesDrawingStrategyBuilder(this.context)
                                                                                        .setRadius(CalibrationConfiguration.RADIUS)
                                                                                        .setLineWidth(CircleConfiguration.LINE_WIDTH)
                                                                                        .setCirclesInRow(CalibrationConfiguration.CIRCLES_IN_ROW)
                                                                                        .setCirclesInColumn(CalibrationConfiguration.CIRCLES_IN_COLUMN)
                                                                                        .setCanvasWidth((this.canvas.nativeElement as HTMLCanvasElement).width)
                                                                                        .setCanvasHeight((this.canvas.nativeElement as HTMLCanvasElement).height)
                                                                                        .build(), this.circles);
        this.resetCirclesEvent();
    }

    private attachCirclesEvent(): void {
        (this.canvas.nativeElement as HTMLCanvasElement).addEventListener('click', this.onCircleClickEvent);
    }

    private removeCirclesEvent(): void {
        (this.canvas.nativeElement as HTMLCanvasElement).removeEventListener('click', this.onCircleClickEvent);
    }

    private resetCirclesEvent(): void {
        this.removeCirclesEvent();
        this.attachCirclesEvent();
    }

    private onCircleClickEvent = (event: MouseEvent) => {
        var leftContentWidth = this.leftMenuContentOpened ? ComponentsSizes.LEFT_CONTENT_WIDTH : 0;
        var topContentHeight = ComponentsSizes.TOP_CONTENT_HEIGHT;
        this.circles.forEach(circle => {
            if(this.isIntersect(event.clientX - leftContentWidth, event.clientY - topContentHeight, circle) && circle.enable){
                this.trackerActionService.sendDetectedEyesRequest(new EyeDataRequest(circle.id, true));
            }
        });
    }

    private isIntersect(mousePositionX: number, mousePositionY: number, circle: Circle): boolean {
        return Math.sqrt((mousePositionX-circle.x) ** 2 + (mousePositionY - circle.y) ** 2) < circle.radius;
    }

    private activateCircles(): void {
        this.circles.forEach(circle => {
            circle.enable = true;
            circle.nextColor();
        });
    }

    private deactivateCricles(): void {
        this.circles.forEach(circle => {
            circle.enable = false;
            circle.resetToDisabledColor();
        });
    }

    private drawCircles(): void {
        this.circles.forEach(circle => circle.draw());
    }

    private listenOnHideLeftContentEvent(): void {
        this.hideLeftContentSubscription = this.topContentActionService.getAction().subscribe(componentAction => {   
            this.handleTopContentEvent(componentAction.action, componentAction.value);
        });
    }

    private handleTopContentEvent(action: any, value: any): void {
        switch(action){       
            case TopContentActions.UPDATE_LEFT_CONTENT:
                this.leftMenuContentOpened = value;
                this.clearCanvas();
                this.loading = true;
                this.trackerService.ping().subscribe(
                    data => { 
                        this.handleSuccessPingAfterTopContentEvent(); 
                        this.loading = false;
                    },
                    error => { 
                        this.handleErrorPingAfterTopContentEvent(); 
                        this.loading = false;
                    }
                );         
            break;
            case TopContentActions.OPEN_LEFT_CONTENT:
                this.leftMenuContentOpened = value;
            break;
        }
    }

    private handleSuccessPingAfterTopContentEvent(){
        if(this.leftMenuContentOpened){   
            setTimeout(() => this.showMessage(this.appConfiguration.getComponentsMessage('tracker.modules.middlecontent.calibration.text.cannotstart'), 
                                              this.appConfiguration.getComponentsMessage('tracker.modules.middlecontent.calibration.title.cannotstart')));
            this.leftContentOpened();
        }else{
            this.leftContentClosed();
        } 
    }

    private handleErrorPingAfterTopContentEvent(){
        this.topContentActionService.sendAction(new ComponentAction(TopContentActions.OPEN_LEFT_CONTENT, true));
        this.leftContentOpened();
        setTimeout(() => this.showMessage(this.appConfiguration.getComponentsMessage('tracker.service.server.conncetion.text.error'), 
                                              this.appConfiguration.getComponentsMessage('tracker.service.server.conncetion.title.error')));
    }

    private leftContentOpened(): void {     
        this.changeCanvasSize(window.innerWidth - ComponentsSizes.LEFT_CONTENT_WIDTH, window.innerHeight - ComponentsSizes.TOP_CONTENT_HEIGHT);
        this.reorganizeCircles();
        this.deactivateCricles();
        this.drawCircles();
        this.greyScaleEyes = [];
        this.trackerActionService.sendAllowDataEmit(false);
    }

    private leftContentClosed(): void {
        this.changeCanvasSize(window.innerWidth, window.innerHeight - ComponentsSizes.TOP_CONTENT_HEIGHT);
        this.reorganizeCircles();
        this.activateCircles();
        this.drawCircles();
        this.trackerActionService.sendAllowDataEmit(true);
    }

    private listenOnDetectedEyesFeedback(): void {
        this.detectedEyesRequestSubscription = this.trackerActionService.getDetectedEyes().subscribe(greyScaleEye => {
            var circleId = greyScaleEye.eyeId;
            
            if(this.circles[circleId].hasNextColor() && this.circles[circleId].enable){
                this.circles[circleId].nextColor();
            }else{
                this.circles[circleId].enable = false;
                this.circles[circleId].resetToDisabledColor();
                this.finishedCirclesCounter++;
            }
            this.circles[circleId].draw();
            this.greyScaleEyes.push(greyScaleEye);

            if(this.calibrationFinished()){
                this.finishedCirclesCounter = 0;     
                this.trackerService.ping().subscribe(
                    data => { 
                        if(this.applicationConfiguration.collectingEyeDataMethod == CollectingEyeDataMethod.MERGED){
                            this.trackerService.saveMergedGreyScaleEyes(this.greyScaleEyes).subscribe(
                                data => {
                                    this.resetCalibrationData();
                                    this.handleSuccesEyeDataSend();
                                },
                                error => {
                                    this.resetCalibrationData();
                                }
                            );
                        }else{
                            this.trackerService.saveGreyScaleEyes(this.greyScaleEyes).subscribe(
                                data => {
                                    this.resetCalibrationData();
                                    this.handleSuccesEyeDataSend();
                                },
                                error => {
                                    this.resetCalibrationData(); 
                                }
                            );
                        }        
                    },
                    error => { 
                        this.loading = false;
                        this.handleErrorEyeDataSend(); 
                    }
                );       
            }
        });
    }

    private resetCalibrationData(){
        this.loading = false;                               
        this.greyScaleEyes = [];
    }

    private handleSuccesEyeDataSend(): void {
        this.showMessage(this.appConfiguration.getComponentsMessage('tracker.service.server.eye.send.text.success'), 
                         this.appConfiguration.getComponentsMessage('tracker.service.server.eye.send.title.success'));
    }

    private handleErrorEyeDataSend(): void {
        this.showMessage(this.appConfiguration.getComponentsMessage('tracker.service.server.eye.send.text.error'), 
                         this.appConfiguration.getComponentsMessage('tracker.service.server.eye.send.title.error'));
    }

    private calibrationFinished(): boolean{  
        return this.finishedCirclesCounter === (CalibrationConfiguration.CIRCLES_IN_COLUMN * CalibrationConfiguration.CIRCLES_IN_ROW);
    }
}
