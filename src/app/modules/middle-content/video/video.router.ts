import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { VideoComponent } from "./video.component";

export const router: Routes = [
    { path: '', component: VideoComponent } 
  ];
  
export const VideoRouter: ModuleWithProviders = RouterModule.forChild(router);