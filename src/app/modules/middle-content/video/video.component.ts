import { Component, AfterViewInit, OnDestroy, NgZone, OnInit } from '@angular/core';

import { Tracker } from '../../../models/tracker/tracker';
import { VideoConfiguration } from './video-configuration';
import { DataSendStartegy } from '../../../models/tracker/data-send-strategy';
import { TrackerActionService } from '../../../services/tracker-action-service/tracker-action.service';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit, AfterViewInit, OnDestroy {
    private tracker: Tracker;

    constructor(private ngZone: NgZone){}

    ngOnInit(): void {
        this.tracker = new Tracker(VideoConfiguration.VIDEO_ID, VideoConfiguration.CANVAS_ID);
    }

    ngAfterViewInit(): void {
        this.ngZone.runOutsideAngular(()=>{
            this.tracker.run(DataSendStartegy.NONE, true);
        });
    }

    ngOnDestroy(): void { 
        this.tracker.shutdown();  
    }
}
