import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoComponent } from './video.component';
import { VideoRouter } from './video.router';

@NgModule({
  imports: [
    CommonModule,
    VideoRouter
  ],
  declarations: [VideoComponent],
  exports: [VideoComponent]
})
export class VideoModule { }
