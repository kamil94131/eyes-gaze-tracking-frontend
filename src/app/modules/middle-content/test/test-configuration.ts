export abstract class TestConfiguration {
    public static readonly RECTANGLES_IN_ROW: number = 3;
    public static readonly RECTANGLES_IN_COLUMN: number = 3;
    public static readonly VIDEO_ID = 'video';
    public static readonly CANVAS_ID = 'canvas'; 
    public static readonly MESSAGE_DURATION = 1500;
    public static readonly UNKNOW_EYE = -1;
    public static readonly DISPLAY_RECTANGLE_TIME = 500;
    public static readonly RE_DRAW_TIMEOUT = 10;
    public static readonly ONE_SECOND = 1000;
}