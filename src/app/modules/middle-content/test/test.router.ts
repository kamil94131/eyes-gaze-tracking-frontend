import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { TestComponent } from "./test.component";

export const router: Routes = [
    { path: '', component: TestComponent } 
  ];
  
export const TestRouter: ModuleWithProviders = RouterModule.forChild(router);