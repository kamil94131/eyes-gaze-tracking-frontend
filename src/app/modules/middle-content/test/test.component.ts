import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, NgZone, ViewEncapsulation, OnDestroy } from '@angular/core';

import { ComponentsSizes } from '../../../utils/configuration/components-sizes';
import { Rectangle } from '../../../models/canvas/rectangle/rectangle';
import { Subscription } from 'rxjs/Subscription';
import { TopContentActionService } from '../../../services/top-content-action-service/top-content-action.service';
import { TopContentActions } from '../../../utils/component-actions/top-content-actions';
import { Tracker } from '../../../models/tracker/tracker';
import { MatSnackBar } from '@angular/material';
import { TestConfiguration } from './test-configuration';
import { TrackerActionService } from '../../../services/tracker-action-service/tracker-action.service';
import { DataSendStartegy } from '../../../models/tracker/data-send-strategy';
import { RectanglesArrangementGenerator } from '../../../models/canvas/rectangle/generator/rectangles-arrangement-generator';
import { RectanglesDrawingStrategyBuilder } from '../../../models/canvas/rectangle/generator/drawing-strategy/rectangles-drawing-strategy-builder';
import { RectangleConfiguration } from '../../../models/canvas/rectangle/rectangle-configuration';
import { TrackerService } from '../../../services/tracker-service/tracker.service';
import { EyeDataRequest } from '../../../services/tracker-action-service/eye-data-request';
import { AppConfiguration } from '../../../services/app-configuration-service/app-configuration';
import { Observable, Subject } from 'rxjs';
import { ComponentAction } from '../../../services/component-action';

@Component({
    selector: 'app-test',
    templateUrl: './test.component.html',
    styleUrls: ['./test.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class TestComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('rectanglesCanvas') canvas: ElementRef;
    private context: CanvasRenderingContext2D; 
    private tracker: Tracker;
    private hideLeftContentSubscription: Subscription;
    private detectedEyesConstantSubscription: Subscription;
    private rectangles: Array<Rectangle> = new Array<Rectangle>();
    private timer: Observable<any> = Observable.timer(TestConfiguration.DISPLAY_RECTANGLE_TIME);
    private timerSubscription: Subscription;
    private lastDrawnRectangleIndex: number;
    private loading: boolean = false;
    private startTime: any = 0;
    private time: any = 0;
    private fps: any = 0;
    private frames: number = 0;
    
    constructor(private ngZone: NgZone, 
                private topContentActionService: TopContentActionService, 
                public message: MatSnackBar, 
                private trackerActionService: TrackerActionService,
                private trackerService: TrackerService,
                private appConfiguration: AppConfiguration) {
    }

    ngOnInit() {
        this.tracker = new Tracker(TestConfiguration.VIDEO_ID, TestConfiguration.CANVAS_ID, this.trackerActionService);
        this.context = (this.canvas.nativeElement as HTMLCanvasElement).getContext("2d");
        this.listenOnHideLeftContent();
        this.listenOnDetectedEyesFeedback();
        this.trackerActionService.sendDetectedEyesRequest(new EyeDataRequest(TestConfiguration.UNKNOW_EYE, false));  
    }

    ngAfterViewInit(): void {
        setTimeout(() => this.showMessage(this.appConfiguration.getComponentsMessage('tracker.modules.middlecontent.test.text.cannotstart'), 
                                          this.appConfiguration.getComponentsMessage('tracker.modules.middlecontent.test.title.cannotstart')));
        this.changeCanvasSize(window.innerWidth - ComponentsSizes.LEFT_CONTENT_WIDTH, window.innerHeight - ComponentsSizes.TOP_CONTENT_HEIGHT + 1);
        this.createNewRectangles();
        this.drawRectangles();
        this.tracker.run(DataSendStartegy.CONSTANT, false);
    }

    ngOnDestroy(): void {
        this.message.dismiss();
        this.hideLeftContentSubscription.unsubscribe();
        this.detectedEyesConstantSubscription.unsubscribe();
        this.tracker.shutdown();       
    }

    private showMessage(text: string, label: string): void{
        this.message.open(text, label, { duration: TestConfiguration.MESSAGE_DURATION });
    }

    private changeCanvasSize(canvasWidth: number, canvasHeight: number){
        (this.canvas.nativeElement as HTMLCanvasElement).width  = canvasWidth;
        (this.canvas.nativeElement as HTMLCanvasElement).height = canvasHeight;
    }

    private clearCanvas(): void {
        this.context.clearRect(0, 0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);
    }

    private createNewRectangles(): void {
        this.rectangles = RectanglesArrangementGenerator.generateRectangles(new RectanglesDrawingStrategyBuilder(this.context)
                                                                                                                .setLineWidth(RectangleConfiguration.LINE_WIDTH)
                                                                                                                .setRectanglesInRow(TestConfiguration.RECTANGLES_IN_ROW)
                                                                                                                .setRectanglesInColumn(TestConfiguration.RECTANGLES_IN_COLUMN)
                                                                                                                .setCanvasWidth((this.canvas.nativeElement as HTMLCanvasElement).width)
                                                                                                                .setCanvasHeight((this.canvas.nativeElement as HTMLCanvasElement).height)                                                                                                            
                                                                                                                .build());
    }

    private reorganizeRectangles(): void {
        this.clearCanvas();
        this.rectangles = RectanglesArrangementGenerator.generateRectangles(new RectanglesDrawingStrategyBuilder(this.context)
                                                                                                                .setLineWidth(RectangleConfiguration.LINE_WIDTH)
                                                                                                                .setRectanglesInRow(TestConfiguration.RECTANGLES_IN_ROW)
                                                                                                                .setRectanglesInColumn(TestConfiguration.RECTANGLES_IN_COLUMN)
                                                                                                                .setCanvasWidth((this.canvas.nativeElement as HTMLCanvasElement).width)
                                                                                                                .setCanvasHeight((this.canvas.nativeElement as HTMLCanvasElement).height)                                                                                                            
                                                                                                                .build());
    }

    private drawRectangles(): void {
        this.rectangles.forEach(rectangle => rectangle.draw());
    }

    private listenOnHideLeftContent(): void {
        this.hideLeftContentSubscription = this.topContentActionService.getAction().subscribe(componentAction => {   
            this.handleTopContentAction(componentAction.action, componentAction.value);
        });
    }

    private handleTopContentAction(action: any, value: any): void {
        switch(action){       
            case TopContentActions.UPDATE_LEFT_CONTENT:
                const leftMenuContentOpened: boolean = value;
                this.clearCanvas();
                this.loading = true;
                this.trackerService.ping().subscribe(
                    data => { 
                        this.trackerService.isNeuralNetworkModelTrained().subscribe(
                            data => {
                                if(!(<boolean>data)){
                                    this.handleNeuralNetworkIsNotTrained(); 
                                    this.loading = false;  
                                }else{
                                    this.handleSuccessPingAfterTopContentEvent(leftMenuContentOpened); 
                                    this.loading = false;
                                }
                            }, error => {
                                this.handleErrorPingAfterTopContentEvent(); 
                                this.loading = false;
                            }
                        )     
                    },
                    error => { 
                        this.handleErrorPingAfterTopContentEvent(); 
                        this.loading = false;
                    }
                );    
            break;
        }
    }

    private handleNeuralNetworkIsNotTrained(){
        this.topContentActionService.sendAction(new ComponentAction(TopContentActions.OPEN_LEFT_CONTENT, true));
        this.leftContentOpened();
        setTimeout(() => this.showMessage(this.appConfiguration.getComponentsMessage('tracker.service.server.model.not.trained.text.error'), 
                                              this.appConfiguration.getComponentsMessage('tracker.service.server.model.not.trained.title.error')));
    }

    private handleSuccessPingAfterTopContentEvent(leftMenuContentOpened: boolean){
        if(leftMenuContentOpened){   
            setTimeout(() => this.showMessage(this.appConfiguration.getComponentsMessage('tracker.modules.middlecontent.calibration.text.cannotstart'), 
                                              this.appConfiguration.getComponentsMessage('tracker.modules.middlecontent.calibration.title.cannotstart')));
            this.leftContentOpened();
        }else{
            this.leftContentClosed();
        } 
    }

    private handleErrorPingAfterTopContentEvent(){
        this.topContentActionService.sendAction(new ComponentAction(TopContentActions.OPEN_LEFT_CONTENT, true));
        this.leftContentOpened();
        setTimeout(() => this.showMessage(this.appConfiguration.getComponentsMessage('tracker.service.server.conncetion.text.error'), 
                                              this.appConfiguration.getComponentsMessage('tracker.service.server.conncetion.title.error')));
    }

    private leftContentOpened(): void {
        this.changeCanvasSize(window.innerWidth - ComponentsSizes.LEFT_CONTENT_WIDTH, window.innerHeight - ComponentsSizes.TOP_CONTENT_HEIGHT);
        this.reorganizeRectangles();
        this.deactivateRectangles();
        this.trackerActionService.sendAllowDataEmit(false);
        setTimeout(() => { this.drawRectangles() }, TestConfiguration.RE_DRAW_TIMEOUT);
    }

    private leftContentClosed(): void {
        this.changeCanvasSize(window.innerWidth, window.innerHeight - ComponentsSizes.TOP_CONTENT_HEIGHT);
        this.reorganizeRectangles();
        this.activateRectangles();
        this.trackerActionService.sendAllowDataEmit(true);
        setTimeout(() => { this.drawRectangles() }, TestConfiguration.RE_DRAW_TIMEOUT);
    }

    private activateRectangles(): void {
        this.rectangles.forEach(rectangle => {
            rectangle.enable = true;
            rectangle.visible = false;
            rectangle.nextColor();
        });
    }

    private deactivateRectangles(): void {
        this.rectangles.forEach(rectangle => {
            rectangle.enable = false;
            rectangle.visible = true;
            rectangle.resetToDisabledColor();
        });
    }

    private listenOnDetectedEyesFeedback(): void {
        this.detectedEyesConstantSubscription = this.trackerActionService.getDetectedEyes().subscribe(greyScaleEye => {
            this.trackerService.predictGazePosition(greyScaleEye).subscribe(
                data => {
                    this.calculateFPS();
                    var rectangleId: number = <number>data;
                    if(rectangleId != TestConfiguration.UNKNOW_EYE){
                        if(this.lastDrawnRectangleIndex != undefined){
                            this.rectangles[this.lastDrawnRectangleIndex].clear();
                        }
                        
                        this.lastDrawnRectangleIndex = rectangleId;
                        this.markObservedArea(rectangleId);
                        this.releaseMarkedAreaAfterTimeout();
                    }
                },
                error => {}
            );
        });
    }

    private calculateFPS(): void {
        if(this.startTime == 0){
            this.startTime = performance.now();
        }else{
            if(performance.now() - this.startTime > TestConfiguration.ONE_SECOND){
                this.fps = this.frames;
                this.frames = 0;
                this.time = 0;
                this.startTime = 0
            }else{
                this.frames++;
            }
        }
    }

    private markObservedArea(rectangleId: number): void {
        this.rectangles[rectangleId].visible = true;
        this.rectangles[rectangleId].draw();
    }

    private releaseMarkedAreaAfterTimeout(): void {
        this.timerSubscription = this.timer.subscribe(() => {
            if(this.rectangles[this.lastDrawnRectangleIndex].enable){
                this.rectangles[this.lastDrawnRectangleIndex].visible = false;
                this.rectangles[this.lastDrawnRectangleIndex].clear();    
            }
        });
    }
}
