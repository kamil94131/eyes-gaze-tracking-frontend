import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestComponent } from './test.component';
import { TestRouter } from './test.router';
import { MatSnackBarModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    TestRouter,
    MatSnackBarModule,
    FormsModule
  ], 
  declarations: [TestComponent],
  exports: [TestComponent]
})
export class TestModule { }
