import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrossValidationComponent } from './cross-validation.component';
import { CrossValidationRouter } from './cross-validation.router';
import { MatSnackBarModule } from '@angular/material';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  imports: [
    CommonModule,
    CrossValidationRouter,
    MatSnackBarModule,
    MatGridListModule,
    MatCardModule,
    MatCheckboxModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule
  ],
  declarations: [CrossValidationComponent],
  exports: [CrossValidationComponent]
})
export class CrossValidationModule { }
