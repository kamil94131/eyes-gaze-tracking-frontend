import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { CrossValidationComponent } from "./cross-validation.component"

export const router: Routes = [
    { path: '', component: CrossValidationComponent } 
  ];
  
export const CrossValidationRouter: ModuleWithProviders = RouterModule.forChild(router);