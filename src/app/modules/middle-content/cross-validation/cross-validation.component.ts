import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { TrackerService } from '../../../services/tracker-service/tracker.service';
import { CrossValidationResult } from '../../../models/tracker/cross-valition-result';
import { CrossValidationConfiguration } from './corss-validation-configuration';
import { AppConfiguration } from '../../../services/app-configuration-service/app-configuration';

@Component({
    selector: 'app-cross-validation',
    templateUrl: './cross-validation.component.html',
    styleUrls: ['./cross-validation.component.css']
})
export class CrossValidationComponent implements OnInit, AfterViewInit {
    private loading: boolean = false;
    private foldersNames: Array<number> = new Array<number>();
    private crossValidationResults: Array<CrossValidationResult> = new Array<CrossValidationResult>();
    private headerLabels: Array<string> = new Array<string>('folderId', 'folderQuantity', 'positiveEvaluation', 'negativeEvaluation');

    constructor(public message: MatSnackBar, private trackerService: TrackerService, private appConfiguration: AppConfiguration) {}

    ngOnInit() {}

    ngAfterViewInit(): void {
        this.getFoldersNames();
    }

    private getFoldersNames(): void {
        this.trackerService.ping().subscribe(
            data => {
                this.trackerService.getMergedEyesGeneratedFolders().subscribe(
                    data =>{
                        this.foldersNames = <Array<number>>data;
                    },
                    error => {
                        this.handleProblemWithMergedEyesFolderOccured();
                        this.loading = false;
                    }
                )
            },
            error => {
                this.handleErrorPing();
                this.loading = false;
            }
        )  
    } 

    private getCrossValidationResult(): void {
        this.loading = true;
        this.trackerService.ping().subscribe(
            data => {
                this.trackerService.isNeuralNetworkModelTrained().subscribe(
                    data => {
                        if(<boolean>data){
                            this.trackerService.getCrossValidationResult().subscribe(
                                data =>{
                                    this.crossValidationResults = <Array<CrossValidationResult>>data;
                                    this.loading = false;
                                },
                                error => { 
                                    this.loading = false;
                                }
                            )
                        }else{
                            this.handleNeuralNetworkIsNotTrained();
                            this.loading = false;
                        }
                    }, 
                    error => {
                        this.handleErrorPing();
                        this.loading = false;
                    }
                )              
            },
            error =>{
                this.handleErrorPing();
                this.loading = false;
            }
        )
    }

    private onBeginCrossValidationClick(): void {
        this.getCrossValidationResult();
    }

    private showMessage(text: string, label: string): void {
        this.message.open(text, label, { duration: CrossValidationConfiguration.MESSAGE_DURATION });
    }

    private handleErrorPing(): void {
        setTimeout(() => this.showMessage(this.appConfiguration.getComponentsMessage('tracker.service.server.conncetion.text.error'), 
                                              this.appConfiguration.getComponentsMessage('tracker.service.server.conncetion.title.error')));
    }

    private handleNeuralNetworkIsNotTrained(){
        setTimeout(() => this.showMessage(this.appConfiguration.getComponentsMessage('tracker.service.server.model.not.trained.text.error'), 
                                              this.appConfiguration.getComponentsMessage('tracker.service.server.model.not.trained.title.error')));
    }

    private handleProblemWithMergedEyesFolderOccured(){
        setTimeout(() => this.showMessage(this.appConfiguration.getComponentsMessage('tracker.service.server.folder.not.found.text.error'), 
                                              this.appConfiguration.getComponentsMessage('tracker.service.server.folder.not.found.title.error')));
    }
}
