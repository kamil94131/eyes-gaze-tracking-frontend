import { ConfigurationComponent } from "./configuration.component";
import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";

export const router: Routes = [
    { path: '', component: ConfigurationComponent } 
  ];
  
export const ConfigurationRouter: ModuleWithProviders = RouterModule.forChild(router);