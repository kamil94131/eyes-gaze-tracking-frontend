import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';

import { TrackerService } from '../../../services/tracker-service/tracker.service';
import { AppConfiguration } from '../../../services/app-configuration-service/app-configuration';
import { MatSnackBar } from '@angular/material';
import { ConfigurationConfiguration } from './configuration-configuration';
import { NeuralNetworkConfiguration } from '../../../utils/configuration/neural-network-configuration';
import { ApplicationConfiguration } from '../../../utils/configuration/application-configuration';
import { MergeDirection } from '../../../utils/configuration/merge-direction';
import { CollectingEyeDataMethod } from '../../../utils/configuration/collecing-eye-data-method';


@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ConfigurationComponent implements OnInit, OnDestroy {
    private neuralNetworkConfiguration: NeuralNetworkConfiguration = new NeuralNetworkConfiguration();
    private applicationConfiguration: ApplicationConfiguration = new ApplicationConfiguration();
    private channels: Array<number> = new Array<number>(1, 3);
    private mergeDirections: any = MergeDirection;
    private collectingEyeDataMethods: any = CollectingEyeDataMethod;
    private color = 'primary';
    
    constructor(private trackerService: TrackerService,
                private appConfiguration: AppConfiguration,
                public message: MatSnackBar) {
    }

    ngOnInit() {
        this.getNeuralNetworkConfiguration();
        this.getApplicationConfiguration();
    }

    ngOnDestroy() {
        this.message.dismiss();
    }

    private getNeuralNetworkConfiguration(): void {
        this.trackerService.ping().subscribe(
            data => {
                this.trackerService.getNeuralNetworkConfiguration().subscribe(
                    data => { this.neuralNetworkConfiguration.copy(<NeuralNetworkConfiguration>data); },
                    error => { this.handleErrorPing(); }
                )
            },
            error => { this.handleErrorPing(); }
        );
    }

    private getApplicationConfiguration(): void {
        this.trackerService.ping().subscribe(
            data => {
                this.trackerService.getApplicationConfiguration().subscribe(
                    data => { this.applicationConfiguration.copy(<ApplicationConfiguration>data); },
                    error => { this.handleErrorPing(); }
                )
            },
            error => { this.handleErrorPing(); }
        );
    }

    private showMessage(text: string, label: string): void {
        this.message.open(text, label, { duration: ConfigurationConfiguration.MESSAGE_DURATION });
    }

    public onStartLearningButtonClick(): void{
        this.trackerService.ping().subscribe(
            data => {
                this.trackerService.startLearningProcess().subscribe(
                    data => {},
                    error => { this.handleErrorPing(); }
                );
            },
            error => { this.handleErrorPing(); }
        );
    }

    public onLoadNeuralNetworkButtonClick(): void {
        this.trackerService.ping().subscribe(
            data => {
                this.trackerService.loadLearnedNeuralNetwork().subscribe(
                    data => {},
                    error => { this.handleErrorPing(); }
                );
            },
            error => { this.handleErrorPing(); }
        );       
    } 
    
    public onSaveNeuralNetworkConfigurationButtonClick(): void {
        this.trackerService.ping().subscribe(
            data => {
                this.trackerService.changeNeuralNetworkConfiguration(this.neuralNetworkConfiguration).subscribe(
                    data => { this.handleSuccessfullySavedChanges(); },
                    error => { this.handleErrorSaveChanges(); }
                );
            },
            error => { this.handleErrorPing(); }
        );      
    }

    public onSaveApplicationConfigurationButtonClick(): void {
        this.trackerService.ping().subscribe(
            data => {
                this.trackerService.changeApplicationConfiguration(this.applicationConfiguration).subscribe(
                    data => { this.handleSuccessfullySavedChanges(); },
                    error => { this.handleErrorSaveChanges(); }
                );
            },
            error => { this.handleErrorPing(); }
        );
    }

    private handleErrorSaveChanges(): void {
        setTimeout(() => this.showMessage(this.appConfiguration.getComponentsMessage('tracker.service.server.configuration.send.text.error'), 
                                              this.appConfiguration.getComponentsMessage('tracker.service.server.configuration.send.title.error')));
    }

    private handleSuccessfullySavedChanges(): void {
        setTimeout(() => this.showMessage(this.appConfiguration.getComponentsMessage('tracker.service.server.configuration.send.text.success'), 
                                              this.appConfiguration.getComponentsMessage('tracker.service.server.configuration.send.title.success')));
    }

    private handleErrorPing(): void {
        setTimeout(() => this.showMessage(this.appConfiguration.getComponentsMessage('tracker.service.server.conncetion.text.error'), 
                                              this.appConfiguration.getComponentsMessage('tracker.service.server.conncetion.title.error')));
    }
}
