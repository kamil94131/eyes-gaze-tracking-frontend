import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";

export const router: Routes = [];
  
export const AppRouter: ModuleWithProviders = RouterModule.forRoot(router);