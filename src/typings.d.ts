/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

declare var tracking: any;
declare var eye: any;
declare var face: any;

declare module "*.json" {
  const value: any;
  export default value;
}

